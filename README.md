# psyq2elf

psyq2elf is a set of tools to convert PSX Psy-Q SDK libraries to ELF format.
The converted libraries can be used with modern versions of GCC to build PSX
executables.

The tools have been tested with Psy-Q SDK version 4.6.

## Requirements
A Linux environment with make and a C compiler is required. On Windows 10,
Windows Subsystem for Linux can be used. Cygwin may also work but hasn't
been tested.

## Dependencies
Install the following packages:
```
libgmp-dev libmpfr-dev libmpc-dev texinfo
```

## Convert libraries
```
# Build psyq2elf
make
# Convert Psy-Q libraries
./psyqlib.sh <path to Psy-Q SDK>/lib <path to new SDK>/lib
# Copy and rename header files
./psyqinc.sh <path to Psy-Q SDK>/include <path to new SDK>/include
```

## Building PSX executables
To build PSX executables, you'll need the mipsel-unknown-elf toolchain. It
isn't commonly available as a prebuilt toolchain so you'll need to build it
from source.

### Build binutils
Download a recent version of binutils from https://ftp.gnu.org/gnu/binutils/
and build in a separate build directory:
```
<path to binutils source>/configure --prefix=/usr/local/mipsel-unknown-elf \
--target=mipsel-unknown-elf --with-float=soft
make -j$(nproc)
# May require root privileges
make install
```

### Build gcc
Download a recent version of gcc from https://ftp.gnu.org/gnu/gcc/ and build
in a separate build directory:
```
<path to gcc source>/configure --prefix=/usr/local/mipsel-unknown-elf \
--target=mipsel-unknown-elf --enable-languages=c,c++ --disable-lns \
--disable-libquadmath --disable-libada --disable-libssp \
--disable-libstdcxx --with-float=soft --with-gnu-as --with-gnu-ld
make -j$(nproc)
# May require root privileges
make install
```

### Build examples
```
# Add binutils and gcc binaries to PATH
export PATH="/usr/local/mipsel-unknown-elf/bin:$PATH"
# Export converted SDK path
export PSYQ=<path to new SDK>
cd examples/cube
make
# Run cube.exe in a PSX emulator
```
Have fun!
