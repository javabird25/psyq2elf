/* See LICENSE file for copyright and license details. */

#ifndef PSYQ2ELF_PSYQ_OBJ_H
#define PSYQ2ELF_PSYQ_OBJ_H

#include <stdint.h>
#include <stdio.h>

#include "slist.h"

#define PSYQ_NAME_MAX	64

typedef enum psyq_symbol_type {
	psyq_symbol_internal,
	psyq_symbol_external,
	psyq_symbol_local,
	psyq_symbol_bss,
} psyq_symbol_type;

typedef enum psyq_patch_type {
	psyq_patch_ref = 2,
	psyq_patch_section_base = 4,
	psyq_patch_section_start = 12,
	psyq_patch_section_end = 22,
	psyq_patch_value = 44,
	psyq_patch_expr = 45,
	psyq_patch_section_size = 46,
} psyq_patch_type;

typedef enum psyq_reloc_type {
	psyq_reloc_word_literal = 16,
	psyq_reloc_function_call = 74,
	psyq_reloc_upper_immediate = 82,
	psyq_reloc_lower_immediate = 84,
} psyq_reloc_type;

typedef struct psyq_sym {
	char name[PSYQ_NAME_MAX];
	uint32_t size;
	uint32_t offset;
	uint16_t symbol;
	uint16_t section;
	psyq_symbol_type type;
} psyq_sym;

SLIST_DECL(psyq_sym);

typedef struct psyq_patch {
	uint32_t value;
	uint16_t offset;
	uint16_t symbol;
	enum psyq_reloc_type reloc_type;
	enum psyq_patch_type type;
} psyq_patch;

SLIST_DECL(psyq_patch);

typedef struct psyq_section {
	slist_psyq_patch patches;
	char name[PSYQ_NAME_MAX];
	uint8_t *code;
	uint32_t code_size;
	uint32_t bss_alloc_size;
	uint32_t real_bss_size;
	uint16_t symbol;
	uint16_t group;
	uint8_t alignment;
} psyq_section;

SLIST_DECL(psyq_section);

typedef struct psyq_link {
	slist_psyq_section sections;
	slist_psyq_sym syms;
} psyq_link;

int psyq_parse_link(FILE *file, psyq_link *link);

#endif
